#include <gtk/gtk.h>
#include <mdnotebook.h>

// taken from fwupd (LGPL-2.1+)
// https://github.com/fwupd/fwupd/blob/e6b487e7627c398d752c0ea4bb49356623021400/libfwupdplugin/fu-common.c#L2072
GBytes* common_bytes_new_offset(GBytes *bytes, gsize offset, gsize length, GError **error) {
	g_return_val_if_fail(bytes != NULL, NULL);
	g_return_val_if_fail(error == NULL || *error == NULL, NULL);

	/* sanity check */
	if (offset + length > g_bytes_get_size(bytes)) {
		g_set_error(error,
			    G_IO_ERROR,
			    G_IO_ERROR_INVALID_DATA,
			    "cannot create bytes @0x%02x for 0x%02x "
			    "as buffer only 0x%04x bytes in size",
			    (guint)offset,
			    (guint)length,
			    (guint)g_bytes_get_size(bytes));
		return NULL;
	}
	return g_bytes_new_from_bytes(bytes, offset, length);
}

#if GTK_CHECK_VERSION(4, 10, 0)
static void save_document_cb_diag_resp(GtkFileDialog* fp, GAsyncResult* res, MdNotebookView* view) {
	GError* err = NULL;
	GFile* file = gtk_file_dialog_save_finish(fp, res, &err);
	if (err) {
		g_critical("Failed saving to file: %s", err->message);
		g_error_free(err);
		return;
	}
#else
static void save_document_cb_diag_resp(GtkNativeDialog* fp, gint response_id, MdNotebookView* view) {
	if (response_id != GTK_RESPONSE_ACCEPT)
		return;

	GFile* file = gtk_file_chooser_get_file(GTK_FILE_CHOOSER(fp));
	GError* err = NULL;
#endif
	GFileOutputStream* out = g_file_replace(file, NULL, FALSE, G_FILE_CREATE_NONE, NULL, &err);
	if (err) {
		fprintf(stderr, "Failure saving file: %s\n", err->message);
		g_error_free(err);
		g_object_unref(file);
		return;
	}

	GBytes* serialized = mdnotebook_view_serialize_buffer(view, MDNOTEBOOK_VIEW_SERIALIZE_TARGET_INTERNAL);

	gsize written_bytes = 0;
	gsize serialized_size = g_bytes_get_size(serialized);

	do {
		g_autoptr(GBytes) serialized_off = NULL;

		serialized_off = common_bytes_new_offset(serialized, written_bytes, serialized_size - written_bytes, &err);
		if (!serialized_off) {
			fprintf(stderr, "Failure saving file: %s\n", err->message);
			g_error_free(err);
			goto free;
		}

		gint n;
		if (n = g_output_stream_write_bytes(G_OUTPUT_STREAM(out), serialized, NULL, &err), n == -1) {
			fprintf(stderr, "Failure saving file: %s\n", err->message);
			g_error_free(err);
			goto free;
		}
		written_bytes += n;
	} while (written_bytes < serialized_size);


	if (!g_output_stream_close(G_OUTPUT_STREAM(out), NULL, &err)) {
		fprintf(stderr, "Failure saving file: %s\n", err->message);
		g_error_free(err);
	}
free:
	g_object_unref(out);
	g_bytes_unref(serialized);
	g_object_unref(file);
}

typedef struct {
	MdNotebookView* view;
	guint8* buffer;
} ReadNoteUd;
static void open_document_cb_diag_resp_read_note(GObject* src, GAsyncResult* res, ReadNoteUd* user_data) {
	gsize bytes_read;
	GError* err = NULL;
	if (!g_input_stream_read_all_finish(G_INPUT_STREAM(src), res, &bytes_read, &err)) {
		g_critical("Failure opening note: %s\n", err->message);
		goto free;
	};

	GBytes* data = g_bytes_new_take(user_data->buffer, bytes_read);
	mdnotebook_view_deserialize_set_buffer(user_data->view, data);

free:
	//g_free(user_data->buffer);
	g_free(user_data);
	g_object_unref(src);
}

#if GTK_CHECK_VERSION(4, 10, 0)
static void open_document_cb_diag_resp(GtkFileDialog* fp, GAsyncResult* res, MdNotebookView* view) {
	GError* err = NULL;
	GFile* file = gtk_file_dialog_open_finish(fp, res, &err);
	if (err) {
		g_critical("Failed opening a file: %s", err->message);
		g_error_free(err);
		return;
	}
#else
static void open_document_cb_diag_resp(GtkNativeDialog* fp, gint response_id, MdNotebookView* view) {
	if (response_id != GTK_RESPONSE_ACCEPT)
		return;

	GFile* file = gtk_file_chooser_get_file(GTK_FILE_CHOOSER(fp));
	GError* err = NULL;
#endif
	GFileInputStream* istream = g_file_read(file, NULL, &err);
	if (err) {
		g_critical("Failure opening note: %s\n", err->message);
		g_error_free(err);
		g_object_unref(file);
	}

	GFileInfo* info = g_file_input_stream_query_info(istream, G_FILE_ATTRIBUTE_STANDARD_SIZE, NULL, &err);
	if (err) {
		g_critical("Failure opening note: %s\n", err->message);
		g_error_free(err);
		g_object_unref(istream);
		g_object_unref(file);
	}
	goffset size = g_file_info_get_size(info);
	g_object_unref(info);

	if (size == 0) {
		GBytes* data = g_bytes_new_static("", 0);
		mdnotebook_view_deserialize_set_buffer(view, data);
		goto done;
	}

	guint8* buffer = g_malloc(size);
	ReadNoteUd* read_note_ud = g_new(ReadNoteUd, 1);
	read_note_ud->view = view;
	read_note_ud->buffer = buffer;
	g_input_stream_read_all_async(G_INPUT_STREAM(istream), buffer, size, G_PRIORITY_HIGH, NULL, (GAsyncReadyCallback)open_document_cb_diag_resp_read_note, read_note_ud);

done:
	g_object_unref(file);
}

typedef struct {
	GtkApplicationWindow* window;
	MdNotebookView* view;
} DocumentActUd;
static void save_document_cb(GSimpleAction*, GVariant*, DocumentActUd* user_data) {
#if GTK_CHECK_VERSION(4, 10, 0)
	GtkFileDialog* fp = gtk_file_dialog_new();
	gtk_file_dialog_set_modal(fp, TRUE);
#else
	GtkFileChooserNative* fp = gtk_file_chooser_native_new("Choose location", GTK_WINDOW(user_data->window), GTK_FILE_CHOOSER_ACTION_SAVE, NULL, NULL);
#endif

	g_autoptr(GtkFileFilter) fp_filter = gtk_file_filter_new();
	gtk_file_filter_set_name(fp_filter, "MdNotebook Markdown");
	gtk_file_filter_add_mime_type(fp_filter, "text/plain");
	gtk_file_filter_add_mime_type(fp_filter, "text/markdown");
	gtk_file_filter_add_mime_type(fp_filter, "text/x-markdown");

#if GTK_CHECK_VERSION(4, 10, 0)
	g_autoptr(GListStore) filters = g_list_store_new(GTK_TYPE_FILE_FILTER);
	g_list_store_append(filters, fp_filter);
	gtk_file_dialog_set_filters(fp, G_LIST_MODEL(filters));

	gtk_file_dialog_save(fp, GTK_WINDOW(user_data->window), NULL, (GAsyncReadyCallback)save_document_cb_diag_resp, user_data->view);
#else
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fp), fp_filter);
	gtk_native_dialog_show(GTK_NATIVE_DIALOG(fp));

	g_signal_connect(fp, "response", G_CALLBACK(save_document_cb_diag_resp), user_data->view);
#endif
}
static void open_document_cb(GtkButton*, DocumentActUd* user_data) {
#if GTK_CHECK_VERSION(4, 10, 0)
	GtkFileDialog* fp = gtk_file_dialog_new();
	gtk_file_dialog_set_modal(fp, TRUE);
#else
	GtkFileChooserNative* fp = gtk_file_chooser_native_new("Choose Note", GTK_WINDOW(user_data->window), GTK_FILE_CHOOSER_ACTION_OPEN, NULL, NULL);
#endif

	g_autoptr(GtkFileFilter) fp_filter = gtk_file_filter_new();
	gtk_file_filter_set_name(fp_filter, "MdNotebook Markdown");
	gtk_file_filter_add_mime_type(fp_filter, "text/plain");
	gtk_file_filter_add_mime_type(fp_filter, "text/markdown");
	gtk_file_filter_add_mime_type(fp_filter, "text/x-markdown");

#if GTK_CHECK_VERSION(4, 10, 0)
	g_autoptr(GListStore) filters = g_list_store_new(GTK_TYPE_FILE_FILTER);
	g_list_store_append(filters, fp_filter);
	//gtk_file_dialog_set_filters(fp, G_LIST_MODEL(filters));

	gtk_file_dialog_open(fp, GTK_WINDOW(user_data->window), NULL, (GAsyncReadyCallback)open_document_cb_diag_resp, user_data->view);
#else
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fp), fp_filter);
	gtk_native_dialog_show(GTK_NATIVE_DIALOG(fp));

	g_signal_connect(fp, "response", G_CALLBACK(open_document_cb_diag_resp), user_data->view);
#endif
}

static void activate(GtkApplication* app, gpointer) {
	GtkWidget *window,*topbar,*actbox,*actprim,*actmenu,*shell,*scroll,*view,*bufwgt,*innerwgt,*btmbar,*toolbar,*zoom_slider;
	GMenu* act;
	GSimpleAction *save_document = g_simple_action_new("save_document", NULL), *about = g_simple_action_new("about", NULL);
	MdNotebookBuffer* buf;
	GtkTextIter iter;
	GtkTextChildAnchor* anch;
	GtkAdjustment* zoom_adj;

	window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "MdNotebook Demo");
	gtk_window_set_default_size(GTK_WINDOW(window), 853, 480);

	g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(save_document));
	g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(about));

	topbar = gtk_header_bar_new();

	actbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_add_css_class(actbox, "linked");

	actprim = gtk_button_new_with_label("Open");
	gtk_widget_add_css_class(actprim, "suggested-action");

	act = g_menu_new();
	g_menu_append(act, "Save Document", "win.save_document");
	g_menu_append(act, "About MdNotebook", "win.about");
	actmenu = gtk_menu_button_new();
	gtk_menu_button_set_direction(GTK_MENU_BUTTON(actmenu), GTK_ARROW_DOWN);
	gtk_menu_button_set_icon_name(GTK_MENU_BUTTON(actmenu), "pan-down-symbolic");
	gtk_menu_button_set_menu_model(GTK_MENU_BUTTON(actmenu), G_MENU_MODEL(act));

	gtk_box_append(GTK_BOX(actbox), actprim);
	gtk_box_append(GTK_BOX(actbox), actmenu);

	gtk_header_bar_pack_start(GTK_HEADER_BAR(topbar), actbox);
	gtk_window_set_titlebar(GTK_WINDOW(window), topbar);

	shell = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

	scroll = gtk_scrolled_window_new();
	gtk_widget_set_hexpand(scroll, TRUE);
	gtk_widget_set_vexpand(scroll, TRUE);

	view = mdnotebook_zoomview_new();
	mdnotebook_view_attach_action_group(MDNOTEBOOK_VIEW(mdnotebook_zoomview_get_textview(MDNOTEBOOK_ZOOMVIEW(view))), GTK_APPLICATION_WINDOW(window));
	buf = MDNOTEBOOK_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(mdnotebook_zoomview_get_textview(MDNOTEBOOK_ZOOMVIEW(view)))));

	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(buf), &iter);
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buf), &iter, "Hello $\\LaTeX$!\n  - MdNotebook Demo", -1);

	btmbar = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_hexpand(btmbar, TRUE);

	toolbar = mdnotebook_toolbar_new_from_view(MDNOTEBOOK_VIEW(mdnotebook_zoomview_get_textview(MDNOTEBOOK_ZOOMVIEW(view))));
	gtk_widget_set_valign(toolbar, GTK_ALIGN_CENTER);
	gtk_widget_set_hexpand(toolbar, TRUE);
	gtk_box_append(GTK_BOX(btmbar), toolbar);

	zoom_adj = gtk_adjustment_new(1., 0., 5., 1., 0., 0.);
	zoom_slider = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, zoom_adj);
	gtk_widget_set_size_request(zoom_slider, 200, -1);
	gtk_scale_add_mark(GTK_SCALE(zoom_slider), 1., GTK_POS_BOTTOM, "100%");
	gtk_scale_add_mark(GTK_SCALE(zoom_slider), 2.5, GTK_POS_BOTTOM, "250%");

	g_object_bind_property(zoom_adj, "value", view, "zoom", G_BINDING_BIDIRECTIONAL);

	gtk_box_append(GTK_BOX(btmbar), zoom_slider);

	bufwgt = mdnotebook_bufwidget_new();
	innerwgt = gtk_button_new_with_label("in View");
	mdnotebook_bufwidget_set_child(MDNOTEBOOK_BUFWIDGET(bufwgt), innerwgt);

	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(buf), &iter);
	anch = gtk_text_buffer_create_child_anchor(GTK_TEXT_BUFFER(buf), &iter);
	gtk_text_view_add_child_at_anchor(GTK_TEXT_VIEW(mdnotebook_zoomview_get_textview(MDNOTEBOOK_ZOOMVIEW(view))), bufwgt, anch);



	gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(scroll), view);

	gtk_box_append(GTK_BOX(shell), scroll);
	gtk_box_append(GTK_BOX(shell), btmbar);

	gtk_window_set_child(GTK_WINDOW(window), shell);

	DocumentActUd* act_cb = g_new(DocumentActUd, 1);
	act_cb->window = GTK_APPLICATION_WINDOW(window);
	act_cb->view = MDNOTEBOOK_VIEW(mdnotebook_zoomview_get_textview(MDNOTEBOOK_ZOOMVIEW(view)));
	g_signal_connect(save_document, "activate", G_CALLBACK(save_document_cb), act_cb);
	g_signal_connect(actprim, "clicked", G_CALLBACK(open_document_cb), act_cb);

#if GTK_CHECK_VERSION(4, 10, 0)
	gtk_window_present(GTK_WINDOW(window));
#else
	gtk_widget_show(window);
#endif
}

int main(int argc, char** argv) {
	GtkApplication* app;
	int status;

#if GLIB_CHECK_VERSION(2, 74, 0)
	GApplicationFlags flags = G_APPLICATION_DEFAULT_FLAGS;
#else
	GApplicationFlags flags = G_APPLICATION_FLAGS_NONE;
#endif
	app = gtk_application_new("arpa.sp1rit.MdNotebookDemo", flags);

	const gchar *delete_selection_accels[] = { "Delete", NULL };
	gtk_application_set_accels_for_action(app, "view.delete-selection", delete_selection_accels);

	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);
	return status;
}
