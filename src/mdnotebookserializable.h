#ifndef __MDNOTEBOOKSERIALIZABLE_H__
#define __MDNOTEBOOKSERIALIZABLE_H__

#include <glib-object.h>
#include <gtk/gtk.h>


G_BEGIN_DECLS

typedef GtkWidget* (*MdNotebookDeserialize) (const gchar* data);

#define MDNOTEBOOK_TYPE_SERIALIZABLE (mdnotebook_serializable_get_type())
G_DECLARE_INTERFACE (MdNotebookSerializable, mdnotebook_serializable, MDNOTEBOOK, SERIALIZABLE, GtkWidget)

struct _MdNotebookSerializableInterface {
	GTypeInterface parent_iface;

	const gchar* (*get_serialized_key) (MdNotebookSerializable* self);
	gchar* (*serialize) (MdNotebookSerializable* self);
	gchar* (*serialize_markdown)(MdNotebookSerializable* self);
};

const gchar* mdnotebook_serializable_get_serialized_key(MdNotebookSerializable* self);
gchar* mdnotebook_serializable_serialize(MdNotebookSerializable* self);
gchar* mdnotebook_serializeable_serialize_markdown(MdNotebookSerializable* self);

G_END_DECLS

#endif // __MDNOTEBOOKSERIALIZABLE
