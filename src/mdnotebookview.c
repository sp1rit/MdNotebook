#include "mdnotebookconfig.h"

#define MDNOTEBOOK_VIEW_EXPOSE_INTERNAS
#include "mdnotebookview.h"
#include "mdnotebookbuffer.h"
#include "mdnotebookbufferextra.h"
#include "mdnotebookunknownwidget.h"
#include "bufitem/mdnotebookbufitem.h"
#include "bufitem/mdnotebookbufitemcodeblock.h"
#include "bufitem/mdnotebookbufitemdynblock.h"
#include "bufitem/mdnotebookbufitemheading.h"
#include "bufitem/mdnotebookbufitemtext.h"

#include "booktool/mdnotebookbooktool.h"
#include "booktool/mdnotebookbooktooleraser.h"
#include "booktool/mdnotebookbooktoolpen.h"
#include "booktool/mdnotebookbooktoolselect.h"
#include "booktool/mdnotebookbooktooltext.h"

#include "bufitem/mdnotebookbufitemcheckmark.h"

#ifdef MDNOTEBOOK_HAVE_LATEX
#include "bufitem/latex/mdnotebookbufitemlatex.h"
#include "bufitem/latex2/mdnotebookbufitemlatextwo.h"
#endif

#include "mdnotebookutils-private.h"

#define _ __attribute__((unused))

static GtkTextBuffer* mdnotebook_view_create_buffer(GtkTextView*) {
	return mdnotebook_buffer_new(NULL);
}

typedef struct {
	gdouble x;
	gdouble y;
} MdNotebookViewPointerPosition;
typedef struct {
	GSimpleActionGroup* actions;
	GdkModifierType modifier_keys;
	guint latest_keyval;
	GListStore* booktools;
	MdNotebookBookTool* active_tool;
	GtkGesture* stylus_gesture;
	MdNotebookViewPointerPosition pointer_pos;
	MdNotebookViewStrokeProxy stroke_proxy;
	GHashTable* deserialize_methods;
} MdNotebookViewPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MdNotebookView, mdnotebook_view, GTK_TYPE_TEXT_VIEW)

enum {
	HORIZONTAL_RESIZE,
	N_SIGNALS
};

static guint mdnotebook_view_signals[N_SIGNALS] = { 0, };

static void mdnotebook_view_stroke_proxy_draw_fun(_ GtkDrawingArea* area, cairo_t* ctx, _ int width, _ int height, MdNotebookView* view) {
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(view));
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(view);

	mdnotebook_stroke_render(priv->stroke_proxy.active, ctx, FALSE);

	mdnotebook_booktool_render_surface(priv->active_tool, ctx, priv->pointer_pos.x, priv->pointer_pos.y);
}

static void mdnotebook_view_dispose(GObject* object) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(MDNOTEBOOK_VIEW(object));

	g_clear_object(&priv->booktools);
	g_clear_object(&priv->actions);

	g_hash_table_unref(g_steal_pointer(&priv->deserialize_methods));

	if (priv->stroke_proxy.active)
		g_free(g_steal_pointer(&priv->stroke_proxy.active));
	if (priv->stroke_proxy.overlay)
		g_object_unref(g_steal_pointer(&priv->stroke_proxy.overlay));

	G_OBJECT_CLASS(mdnotebook_view_parent_class)->dispose(object);
}

static void mdnotebook_view_size_allocate(GtkWidget* widget, int width, int height, int baseline) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(MDNOTEBOOK_VIEW(widget));
	GtkAllocation rect = { .x = 0, .y = 0, .width = width, .height = height };

	GTK_WIDGET_CLASS(mdnotebook_view_parent_class)->size_allocate(widget, width, height, baseline);
	g_signal_emit(widget, mdnotebook_view_signals[HORIZONTAL_RESIZE], 0, width);

	gtk_widget_size_allocate(priv->stroke_proxy.overlay, &rect, baseline);
}

static void mdnotebook_view_fill_clipboard(MdNotebookView* self, const GtkTextIter* start, const GtkTextIter* end) {
	g_autoptr(GBytes) serialized = mdnotebook_view_serialize_buffer_partial(self, start, end, MDNOTEBOOK_VIEW_SERIALIZE_TARGET_INTERNAL);
	g_autoptr(GBytes) plain_markdown = mdnotebook_view_serialize_buffer_partial(self, start, end, MDNOTEBOOK_VIEW_SERIALIZE_TARGET_MARKDOWN);

	GdkContentProvider* providers[] = {
		gdk_content_provider_new_for_bytes("application/mdnotebook", serialized),
		gdk_content_provider_new_for_bytes("text/markdown;charset=utf-8", plain_markdown),
		gdk_content_provider_new_for_bytes("text/markdown", plain_markdown),
		gdk_content_provider_new_for_bytes("text/plain;charset=utf-8", plain_markdown),
		gdk_content_provider_new_for_bytes("text/plain", plain_markdown)
	};

	g_autoptr(GdkContentProvider) combined = gdk_content_provider_new_union(providers, sizeof(providers) / sizeof(GdkContentProvider*));
	gdk_clipboard_set_content(gdk_display_get_clipboard(gtk_widget_get_display(GTK_WIDGET(self))), combined);
}

static void mdnotebook_view_copy_clipboard(GtkTextView* view) {
	GtkTextBuffer* buf = gtk_text_view_get_buffer(view);

	GtkTextIter start,end;
	if (!gtk_text_buffer_get_selection_bounds(buf, &start, &end))
		return;

	mdnotebook_view_fill_clipboard(MDNOTEBOOK_VIEW(view), &start, &end);
}
static void mdnotebook_view_cut_clipboard(GtkTextView* view) {
	GtkTextBuffer* buf = gtk_text_view_get_buffer(view);

	GtkTextIter start,end;
	if (!gtk_text_buffer_get_selection_bounds(buf, &start, &end))
		return;

	gtk_text_buffer_begin_user_action(buf);
	mdnotebook_view_fill_clipboard(MDNOTEBOOK_VIEW(view), &start, &end);
	gtk_text_buffer_delete(buf, &start, &end);
	gtk_text_buffer_end_user_action(buf);
}

void mdnotebook_view_deserialize_to_buffer(MdNotebookView* self, GBytes* data);

static void mdnotebook_view_paste_clipboard_slice_cb(GOutputStream* mem, GAsyncResult* res, MdNotebookView* self) {
	GError* err = NULL;
	gssize written = g_output_stream_splice_finish(mem, res, &err);
	if (err) {
		g_critical("Failed reading from clipboard: %s", err->message);
		g_error_free(err);
		return;
	}
	if (written <= 0)
		return;

	GtkTextBuffer* buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self));
	gtk_text_buffer_begin_user_action(buf);
	GBytes* bytes = g_memory_output_stream_steal_as_bytes(G_MEMORY_OUTPUT_STREAM(mem));
	if (
		g_strcmp0(g_object_get_data(G_OBJECT(mem), "mimetype"), "application/mdnotebook") == 0
#ifdef MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		|| g_strcmp0(g_object_get_data(G_OBJECT(mem), "mimetype"), "text/notekit-markdown") == 0
#endif
	) {
		mdnotebook_view_deserialize_to_buffer(self, bytes);
	} else {
		gsize len;
		const gchar* data = g_bytes_get_data(bytes, &len);
		mdnotebook_util_buffer_insert_at_cursor(buf, data, len);
	}
	gtk_text_buffer_end_user_action(buf);
}
static void mdnotebook_view_paste_clipboard_cb(GdkClipboard* clipboard, GAsyncResult* res, MdNotebookView* self) {
	const gchar* mime = NULL;
	GError* err = NULL;
	GInputStream* stream = gdk_clipboard_read_finish(clipboard, res, &mime, &err);
	if (err) {
		g_critical("Failed accessing clipboard for pasting: %s", err->message);
		g_error_free(err);
		return;
	}

	g_autoptr(GOutputStream) mem = g_memory_output_stream_new_resizable();
	g_object_set_data_full(G_OBJECT(mem), "mimetype", g_strdup(mime), g_free);
	g_output_stream_splice_async(mem, stream, G_OUTPUT_STREAM_SPLICE_CLOSE_SOURCE | G_OUTPUT_STREAM_SPLICE_CLOSE_TARGET, G_PRIORITY_HIGH, NULL, (GAsyncReadyCallback)mdnotebook_view_paste_clipboard_slice_cb, self);
}
static void mdnotebook_view_paste_clipboard(GtkTextView* view) {
	GdkClipboard* clipboard = gdk_display_get_clipboard(gtk_widget_get_display(GTK_WIDGET(view)));
	if (!clipboard)
		return;

	const gchar* formats[] = {
		"application/mdnotebook",
#ifdef MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		"text/notekit-markdown",
#endif // MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		"text/markdown",
		"text/plain;charset=utf-8",
		"text/plain",
		NULL
	};
	gdk_clipboard_read_async(clipboard, formats, G_PRIORITY_HIGH, NULL, (GAsyncReadyCallback)mdnotebook_view_paste_clipboard_cb, MDNOTEBOOK_VIEW(view));
}

static void mdnotebook_view_class_init(MdNotebookViewClass* class) {
	GtkTextViewClass* text_view_class = GTK_TEXT_VIEW_CLASS(class);

	G_OBJECT_CLASS(class)->dispose = mdnotebook_view_dispose;
	GTK_WIDGET_CLASS(class)->size_allocate = mdnotebook_view_size_allocate;
	text_view_class->create_buffer = mdnotebook_view_create_buffer;
	text_view_class->copy_clipboard = mdnotebook_view_copy_clipboard;
	text_view_class->cut_clipboard = mdnotebook_view_cut_clipboard;
	text_view_class->paste_clipboard = mdnotebook_view_paste_clipboard;

	mdnotebook_view_signals[HORIZONTAL_RESIZE] = g_signal_new("horizontal-resize",
		G_TYPE_FROM_CLASS(class), G_SIGNAL_RUN_LAST, 0,
		NULL, NULL, NULL,
		G_TYPE_NONE, 1, G_TYPE_INT);
}

static gboolean mdnotebook_view_key_pressed(_ GtkEventController* ctl, guint keyval, _ guint keycode, GdkModifierType state, MdNotebookView* self) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), FALSE);
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	priv->modifier_keys = state & (GDK_MODIFIER_MASK);
	priv->latest_keyval = keyval;

	return FALSE;
}

static void mdnotebook_view_pointer_motion(_ GtkEventControllerMotion* ctl, gdouble x, gdouble y, MdNotebookView* self) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	priv->pointer_pos.x = x;
	priv->pointer_pos.y = y;
}

static void mdnotebook_view_booktool_gesture_start(MdNotebookView* self, GtkGesture* gest, gdouble x, gdouble y, gdouble pressure) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);
	if (mdnotebook_booktool_gesture_start(priv->active_tool, x, y, pressure)) {
		gtk_gesture_set_state(gest, GTK_EVENT_SEQUENCE_CLAIMED);
	}
}
static void mdnotebook_view_booktool_gesture_end(MdNotebookView* self, GtkGesture* gest, gdouble x, gdouble y, gdouble pressure) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);
	if (mdnotebook_booktool_gesture_end(priv->active_tool, x, y, pressure)) {
		gtk_gesture_set_state(gest, GTK_EVENT_SEQUENCE_CLAIMED);
	}
}
static void mdnotebook_view_booktool_gesture_move(MdNotebookView* self, GtkGesture* gest, gdouble x, gdouble y, gdouble pressure) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);
	if (mdnotebook_booktool_gesture_move(priv->active_tool, x, y, pressure)) {
		gtk_gesture_set_state(gest, GTK_EVENT_SEQUENCE_CLAIMED);
	}
}


static void mdnotebook_view_drag_begin(GtkGestureDrag* gest, gdouble x, gdouble y, MdNotebookView* self) {
	mdnotebook_view_booktool_gesture_start(self, GTK_GESTURE(gest), x, y, 1.0);
}
static void mdnotebook_view_drag_end(GtkGestureDrag* gest, gdouble x_off, gdouble y_off, MdNotebookView* self) {
	gdouble x,y;
	gtk_gesture_drag_get_start_point(gest, &x, &y);
	mdnotebook_view_booktool_gesture_end(self, GTK_GESTURE(gest), x + x_off, y + y_off, 1.0);
}
static void mdnotebook_view_drag_update(GtkGestureDrag* gest, gdouble x_off, gdouble y_off, MdNotebookView* self) {
	gdouble x,y;
	gtk_gesture_drag_get_start_point(gest, &x, &y);
	mdnotebook_view_booktool_gesture_move(self, GTK_GESTURE(gest), x + x_off, y + y_off, 1.0);
}
static void mdnotebook_view_stylus_down(GtkGestureStylus* gest, gdouble x, gdouble y, MdNotebookView* self) {
	gdouble pressure;
		if (!gtk_gesture_stylus_get_axis(gest, GDK_AXIS_PRESSURE, &pressure))
			pressure = 1.0;
		mdnotebook_view_booktool_gesture_start(self, GTK_GESTURE(gest), x, y, pressure);
}
static void mdnotebook_view_stylus_up(GtkGestureStylus* gest, gdouble x, gdouble y, MdNotebookView* self) {
	gdouble pressure;
		if (!gtk_gesture_stylus_get_axis(gest, GDK_AXIS_PRESSURE, &pressure))
			pressure = 1.0;
		mdnotebook_view_booktool_gesture_end(self, GTK_GESTURE(gest), x, y, pressure);
}
static void mdnotebook_view_stylus_move(GtkGestureStylus* gest, gdouble x, gdouble y, MdNotebookView* self) {
	gdouble pressure;
		if (!gtk_gesture_stylus_get_axis(gest, GDK_AXIS_PRESSURE, &pressure))
			pressure = 1.0;
		mdnotebook_view_booktool_gesture_move(self, GTK_GESTURE(gest), x, y, pressure);
}

void mdnotebook_view_add_booktool(MdNotebookView* self, MdNotebookBookTool* tool);
static void mdnotebook_view_init(MdNotebookView* self) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);
	MdNotebookBuffer* buffer = MDNOTEBOOK_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(self)));

	priv->actions = g_simple_action_group_new();

	priv->modifier_keys = 0;
	priv->latest_keyval = 0;

	priv->deserialize_methods = g_hash_table_new(g_str_hash, g_str_equal);

	GtkEventController* keyctl = gtk_event_controller_key_new();
	g_signal_connect(keyctl, "key-pressed", G_CALLBACK(mdnotebook_view_key_pressed), self);
	gtk_widget_add_controller(GTK_WIDGET(self), keyctl);

	MdNotebookBufItem* codeblock = mdnotebook_bufitem_codeblock_new(self);
	MdNotebookBufItem* title = mdnotebook_bufitem_heading_new();

#ifdef MDNOTEBOOK_HAVE_LATEX
	//MdNotebookBufItem* latex = mdnotebook_bufitem_latex_new(self);
	MdNotebookBufItem* latex = mdnotebook_bufitem_latex_two_new(self);
	mdnotebook_buffer_add_bufitem(buffer, latex);
#endif

	MdNotebookBufItem* checkmark = mdnotebook_bufitem_checkmark_new(self);
	MdNotebookBufItem* dynblock = mdnotebook_bufitem_dynblock_new(self);
	MdNotebookBufItem* text = mdnotebook_bufitem_text_new();
	mdnotebook_buffer_add_bufitem(buffer, codeblock);
	mdnotebook_buffer_add_bufitem(buffer, title);
	mdnotebook_buffer_add_bufitem(buffer, checkmark);
	mdnotebook_buffer_add_bufitem(buffer, dynblock);
	mdnotebook_buffer_add_bufitem(buffer, text);

	priv->booktools = g_list_store_new(MDNOTEBOOK_TYPE_BOOKTOOL);
	MdNotebookBookTool* texttool = mdnotebook_booktool_text_new(self);
	MdNotebookBookTool* pentool = mdnotebook_booktool_pen_new(self);
	MdNotebookBookTool* erasertool = mdnotebook_booktool_eraser_new(self);
	MdNotebookBookTool* selecttool = mdnotebook_booktool_select_new(self);
	priv->active_tool = pentool;
	mdnotebook_view_add_booktool(self, texttool);
	mdnotebook_view_add_booktool(self, pentool);
	mdnotebook_view_add_booktool(self, erasertool);
	mdnotebook_view_add_booktool(self, selecttool);

	GtkEventController* motionctl = gtk_event_controller_motion_new();
	g_signal_connect(motionctl, "motion", G_CALLBACK(mdnotebook_view_pointer_motion), self);
	gtk_event_controller_set_propagation_phase(GTK_EVENT_CONTROLLER(motionctl), GTK_PHASE_CAPTURE);
	gtk_widget_add_controller(GTK_WIDGET(self), GTK_EVENT_CONTROLLER(motionctl));

	GtkGesture* stylusctl = gtk_gesture_stylus_new();
	g_signal_connect(stylusctl, "down", G_CALLBACK(mdnotebook_view_stylus_down), self);
	g_signal_connect(stylusctl, "up", G_CALLBACK(mdnotebook_view_stylus_up), self);
	g_signal_connect(stylusctl, "motion", G_CALLBACK(mdnotebook_view_stylus_move), self);
	gtk_widget_add_controller(GTK_WIDGET(self), GTK_EVENT_CONTROLLER(stylusctl));

	priv->stylus_gesture = stylusctl;

	GtkGesture* dragctl = gtk_gesture_drag_new();
	g_signal_connect(dragctl, "drag-begin", G_CALLBACK(mdnotebook_view_drag_begin), self);
	g_signal_connect(dragctl, "drag-end", G_CALLBACK(mdnotebook_view_drag_end), self);
	g_signal_connect(dragctl, "drag-update", G_CALLBACK(mdnotebook_view_drag_update), self);
	gtk_event_controller_set_propagation_phase(GTK_EVENT_CONTROLLER(dragctl), GTK_PHASE_CAPTURE);
	gtk_widget_add_controller(GTK_WIDGET(self), GTK_EVENT_CONTROLLER(dragctl));

	priv->pointer_pos.x = -1;
	priv->pointer_pos.y = -1;

	// BEGIN Stroke Proxy
	priv->stroke_proxy.overlay = gtk_drawing_area_new();
	gtk_drawing_area_set_draw_func(GTK_DRAWING_AREA(priv->stroke_proxy.overlay), (GtkDrawingAreaDrawFunc)mdnotebook_view_stroke_proxy_draw_fun, self, NULL);
	gtk_text_view_add_overlay(GTK_TEXT_VIEW(self), priv->stroke_proxy.overlay, 0, 0);

	mdnotebook_booktool_activated(priv->active_tool, self);

	priv->stroke_proxy.active = mdnotebook_stroke_new(0xff000000);
}

GtkWidget* mdnotebook_view_new(void) {
	return g_object_new(MDNOTEBOOK_TYPE_VIEW, NULL);
}

GtkWidget* mdnotebook_view_new_with_buffer(MdNotebookBuffer* buffer) {
	GtkWidget* view = mdnotebook_view_new();
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(view), GTK_TEXT_BUFFER(buffer));

	return view;
}

GBytes* mdnotebook_view_serialize_buffer_partial(MdNotebookView* self, const GtkTextIter* start, const GtkTextIter* end, MdNotebookViewSerializeTarget target) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), NULL);

	GByteArray* ret = g_byte_array_new();
	if (gtk_text_iter_compare(start, end) == 0)
		return g_byte_array_free_to_bytes(ret);

	GtkTextIter pos0,pos1;
	//gtk_text_buffer_get_start_iter(buf, &pos0);
	pos0 = *start;

	GtkTextChildAnchor* anch;

	do {
		pos1 = pos0;
		gboolean not_end = TRUE;
		while (anch = gtk_text_iter_get_child_anchor(&pos1), anch == NULL) {
			if (not_end = (gtk_text_iter_forward_char(&pos1) && gtk_text_iter_compare(&pos1, end) < 0), !not_end)
				break;
		}
		gchar* text = gtk_text_iter_get_text(&pos0, &pos1);
		g_byte_array_append(ret, (guint8*)text, strlen(text));
		g_free(text);

		pos0 = pos1;

		if (not_end && anch) {
			guint widgets_len;
			GtkWidget** widgets = gtk_text_child_anchor_get_widgets(anch, &widgets_len);
			if (widgets_len) {
				if (MDNOTEBOOK_IS_SERIALIZABLE(widgets[0])) {
					if (target == MDNOTEBOOK_VIEW_SERIALIZE_TARGET_INTERNAL) {
						gchar* serialized = mdnotebook_serializable_serialize((MdNotebookSerializable*)widgets[0]);
						g_byte_array_append(ret, (guint8*)"![](mdnb:", 9);
						const gchar* serialize_type = mdnotebook_serializable_get_serialized_key((MdNotebookSerializable*)widgets[0]);
						g_byte_array_append(ret, (guint8*)serialize_type, strlen(serialize_type));
						g_byte_array_append(ret, (guint8*)":", 1);
						g_byte_array_append(ret, (guint8*)serialized, strlen(serialized));
						g_byte_array_append(ret, (guint8*)")", 1);
						g_free(serialized);
					} else if (target == MDNOTEBOOK_VIEW_SERIALIZE_TARGET_MARKDOWN) {
						gchar* serialized = mdnotebook_serializeable_serialize_markdown((MdNotebookSerializable*)widgets[0]);
						g_byte_array_append(ret, (guint8*)serialized, strlen(serialized));
						g_free(serialized);
					}
				}
			}
		}
	} while (gtk_text_iter_forward_char(&pos0) && gtk_text_iter_compare(&pos0, end) < 0);
	return g_byte_array_free_to_bytes(ret);
}

GBytes* mdnotebook_view_serialize_buffer(MdNotebookView* self, MdNotebookViewSerializeTarget target) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), NULL);
	GtkTextBuffer* buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self));

	GtkTextIter start,end;
	gtk_text_buffer_get_start_iter(buf, &start);
	gtk_text_buffer_get_end_iter(buf, &end);

	return mdnotebook_view_serialize_buffer_partial(self, &start, &end, target);
}

static gboolean mdnotebook_deserialize_mdnotebook_drawing(MdNotebookView* self, GtkTextBuffer* buf, guint8* dataptr, gsize data_len, gsize* cursor, gchar* found) {
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	mdnotebook_util_buffer_insert_at_cursor(buf, (gchar*)&dataptr[*cursor], (gsize)found - ((gsize)dataptr + *cursor));

	gchar* end_serialized = g_strstr_len(&found[9], data_len - ((((gsize)found+9) - (gsize)dataptr) /*+1*/), ")");
	if (!end_serialized)
		return FALSE;

	gchar* after_type = g_strstr_len(&found[9], (gsize)end_serialized - ((gsize)found+9), ":");
	if (!after_type)
		goto skip;

	gsize type_len = (gsize)after_type - ((gsize)found+9);
	gchar* type = g_new(char, type_len + 1);
	memcpy(type, &found[9], type_len);
	type[type_len] = 0x00;

	gsize serialized_len = (gsize)end_serialized - ((gsize)after_type + 1);
	gchar* serialized = g_new(char, serialized_len + 1);
	memcpy(serialized, &after_type[1], serialized_len);
	serialized[serialized_len] = 0x00;

	GtkTextIter insert_cur;
	gtk_text_buffer_get_iter_at_mark(buf, &insert_cur, gtk_text_buffer_get_insert(buf));
	GtkTextChildAnchor* anch = gtk_text_buffer_create_child_anchor(buf, &insert_cur);

	MdNotebookDeserialize method = (MdNotebookDeserialize)g_hash_table_lookup(priv->deserialize_methods, type);
	GtkWidget* w;
	if (method) {
		w = method(serialized);
		g_free(type);
		g_free(serialized);
	} else {
		w = mdnotebook_unknownwidget_new(type, serialized);
	}
	gtk_text_view_add_child_at_anchor(GTK_TEXT_VIEW(self), w, anch);
	mdnotebook_util_widget_show(w);

skip:
	*cursor = ((gsize)end_serialized+1) - (gsize)dataptr;
	return TRUE;
}
#ifdef MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
static gboolean mdnotebook_deserialize_notekit_drawing(GtkTextView* view, GtkTextBuffer* buf, guint8* dataptr, gsize data_len, gsize* cursor, gchar* nk_found) {
	mdnotebook_util_buffer_insert_at_cursor(buf, (gchar*)&dataptr[*cursor], (gsize)nk_found - ((gsize)dataptr + *cursor));

	gchar* end_nk_serialized = g_strstr_len(&nk_found[7], data_len - ((((gsize)nk_found+7) - (gsize)dataptr) /*+1*/), ")");
	if (!end_nk_serialized)
		return FALSE;

	gchar* comma;
	guint64 len = g_ascii_strtoull(&nk_found[7], &comma, 10);
	if (!comma || comma > end_nk_serialized || comma == &nk_found[7])
		goto skip;

	gsize nk_serialized_len = (gsize)end_nk_serialized - ((gsize)comma + 1);
	gchar* nk_serialized = g_new(char, nk_serialized_len + 1);
	memcpy(nk_serialized, &comma[1], nk_serialized_len);
	nk_serialized[nk_serialized_len] = 0x00;

	GtkTextIter insert_cur;
	gtk_text_buffer_get_iter_at_mark(buf, &insert_cur, gtk_text_buffer_get_insert(buf));
	GtkTextChildAnchor* anch = gtk_text_buffer_create_child_anchor(buf, &insert_cur);

	GtkWidget* w = mdnotebook_bounddrawing_new_with_deserialize_notekit(len, nk_serialized);

	gtk_text_view_add_child_at_anchor(view, w, anch);
	mdnotebook_util_widget_show(w);

skip:
	*cursor = ((gsize)end_nk_serialized+1) - (gsize)dataptr;
	return TRUE;
}
#endif // MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
void mdnotebook_view_deserialize_to_buffer(MdNotebookView* self, GBytes* data) {
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	//GtkTextBuffer* buf = mdnotebook_buffer_new(NULL);
	GtkTextBuffer* buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self));
	//gtk_text_buffer_set_text(buf, "", 0);

	gsize data_len;
	guint8* dataptr = g_bytes_unref_to_data(data, &data_len);
	gsize cursor = 0;

	//GtkTextIter end;

	while (cursor/*+1*/ < data_len) {
		gchar* found = g_strstr_len((gchar*)&dataptr[cursor], data_len - (cursor/*+1*/), "![](mdnb:");
#ifdef MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		gchar* nk_found = g_strstr_len((gchar*)&dataptr[cursor], data_len - (cursor/*+1*/), "![](nk:");

		if (found || nk_found) {
			if (found && nk_found) {
				if (nk_found < found) {
					if (!mdnotebook_deserialize_notekit_drawing(GTK_TEXT_VIEW(self), buf, dataptr, data_len, &cursor, nk_found))
						goto free;
				} else {
					if (!mdnotebook_deserialize_mdnotebook_drawing(self, buf, dataptr, data_len, &cursor, found))
						goto free;
				}
			} else if (!found && nk_found) {
				if (!mdnotebook_deserialize_notekit_drawing(GTK_TEXT_VIEW(self), buf, dataptr, data_len, &cursor, nk_found))
					goto free;
			} else if (found && !nk_found) {
				if (!mdnotebook_deserialize_mdnotebook_drawing(self, buf, dataptr, data_len, &cursor, found))
					goto free;
			}
#else // MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		if (found) {
			if (!mdnotebook_deserialize_mdnotebook_drawing(self, buf, dataptr, data_len, &cursor, found))
				goto free;
#endif // MDNOTEBOOK_HAVE_NOTEKIT_COMPAT
		} else {
			//gtk_text_buffer_get_end_iter(buf, &end);
			mdnotebook_util_buffer_insert_at_cursor(buf, (gchar*)&dataptr[cursor], data_len - (cursor/*+1*/));
			goto free;
		}
	}

free:
	//gtk_text_view_set_buffer(GTK_TEXT_VIEW(self), buf);
	g_free(dataptr);
}

void mdnotebook_view_deserialize_set_buffer(MdNotebookView* self, GBytes* data) {
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));

	GtkTextBuffer* buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self));
	gtk_text_buffer_begin_irreversible_action(buf);
	gtk_text_buffer_set_text(buf, "", 0);
	mdnotebook_view_deserialize_to_buffer(self, data);
	gtk_text_buffer_end_irreversible_action(buf);
}

void mdnotebook_view_register_deserialize_method(MdNotebookView* self, const gchar* name, MdNotebookDeserialize method) {
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	g_hash_table_insert(priv->deserialize_methods, (void*)name, (void*)method);
}

GdkModifierType mdnotebook_view_get_modifier_keys(MdNotebookView* self) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), 0);
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	return priv->modifier_keys;
}

guint mdnotebook_view_get_latest_keyval(MdNotebookView* self) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), 0);
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	return priv->latest_keyval;
}

void mdnotebook_view_insert_action(MdNotebookView* self, GAction* action) {
	MdNotebookViewPrivate* priv;
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	priv = mdnotebook_view_get_instance_private(self);

	g_action_map_add_action(G_ACTION_MAP(priv->actions), action);
}
void mdnotebook_view_attach_action_group(MdNotebookView* self, GtkApplicationWindow* win) {
	MdNotebookViewPrivate* priv;
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	priv = mdnotebook_view_get_instance_private(self);

	gtk_widget_insert_action_group(GTK_WIDGET(win), "view", G_ACTION_GROUP(priv->actions));
}

static gboolean mdnotebook_view_cmp_booktool_type(gconstpointer lhs, gconstpointer rhs) {
	return G_OBJECT_TYPE(lhs) == G_OBJECT_TYPE(rhs);
}
void mdnotebook_view_add_booktool(MdNotebookView* self, MdNotebookBookTool* tool) {
	MdNotebookViewPrivate* priv;
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	priv = mdnotebook_view_get_instance_private(self);

	if (g_list_store_find_with_equal_func(priv->booktools, tool, mdnotebook_view_cmp_booktool_type, NULL)) {
		g_warning("%s is already registered in the MdNotebook.View\n", g_type_name(G_OBJECT_TYPE(tool)));
	} else {
		g_list_store_append(priv->booktools, tool);
		mdnotebook_booktool_registered(tool, self);
	}

	g_object_unref(tool);
}

gboolean mdnotebook_view_select_tool(MdNotebookView* self, MdNotebookBookTool* tool) {
	MdNotebookViewPrivate* priv;
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), FALSE);
	priv = mdnotebook_view_get_instance_private(self);

	if (g_list_store_find(priv->booktools, tool, NULL)) {
		mdnotebook_booktool_deactivated(priv->active_tool, self);
		priv->active_tool = tool;
		mdnotebook_booktool_activated(priv->active_tool, self);
		return TRUE;
	} else {
		g_warning("Tried to select unregistered tool %s\n", g_type_name(G_OBJECT_TYPE(tool)));
		return FALSE;
	}
}
static gboolean mdnotebook_view_cmp_booktool_type_t(gconstpointer lhs, gconstpointer rhs) {
	return G_OBJECT_TYPE(lhs) == *(GType*)rhs;
}
gboolean mdnotebook_view_select_tool_by_type(MdNotebookView* self, GType* tool) {
	MdNotebookViewPrivate* priv;
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), FALSE);
	priv = mdnotebook_view_get_instance_private(self);

	guint position;
	if (g_list_store_find_with_equal_func(priv->booktools, tool, mdnotebook_view_cmp_booktool_type_t, &position)) {
		mdnotebook_booktool_deactivated(priv->active_tool, self);
		priv->active_tool = MDNOTEBOOK_BOOKTOOL(g_list_model_get_object(G_LIST_MODEL(priv->booktools), position));
		mdnotebook_booktool_activated(priv->active_tool, self);
		return TRUE;
	} else {
		g_warning("Tried to select unregistered tool %s\n", g_type_name(*tool));
		return FALSE;
	}
}

GListModel* mdnotebook_view_get_tools(MdNotebookView* self) {
	MdNotebookViewPrivate* priv;
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), NULL);
	priv = mdnotebook_view_get_instance_private(self);

	return G_LIST_MODEL(priv->booktools);
}

void mdnotebook_view_set_cursor(MdNotebookView* self, GdkCursor* cursor) {
	MdNotebookViewStrokeProxy* prox = mdnotebook_view_get_stroke_proxy(self);
	if (!prox)
		return;

	gtk_widget_set_cursor(prox->overlay, cursor);
	gtk_widget_set_cursor(GTK_WIDGET(self), cursor);
}
void mdnotebook_view_set_cursor_from_name(MdNotebookView* self, const gchar* cursor) {
	MdNotebookViewStrokeProxy* prox = mdnotebook_view_get_stroke_proxy(self);
	if (!prox)
		return;

	gtk_widget_set_cursor_from_name(prox->overlay, cursor);
	gtk_widget_set_cursor_from_name(GTK_WIDGET(self), cursor);
}

void mdnotebook_view_redraw_overlay(MdNotebookView* self) {
	MdNotebookViewStrokeProxy* stroke_proxy = mdnotebook_view_get_stroke_proxy(self);
	if (!stroke_proxy)
		return;

	gtk_widget_queue_draw(stroke_proxy->overlay);
}

MdNotebookViewStrokeProxy* mdnotebook_view_get_stroke_proxy(MdNotebookView* self) {
	g_return_val_if_fail(MDNOTEBOOK_IS_VIEW(self), NULL);
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	return &priv->stroke_proxy;
}

void mdnotebook_view_set_stylus_gesture_state(MdNotebookView* self, gboolean state) {
	g_return_if_fail(MDNOTEBOOK_IS_VIEW(self));
	MdNotebookViewPrivate* priv = mdnotebook_view_get_instance_private(self);

	if (state)
		gtk_event_controller_set_propagation_phase(GTK_EVENT_CONTROLLER(priv->stylus_gesture), GTK_PHASE_CAPTURE);
	else
		gtk_event_controller_set_propagation_phase(GTK_EVENT_CONTROLLER(priv->stylus_gesture), GTK_PHASE_NONE);
}

