#include "mdnotebookunknownwidget.h"
#include "mdnotebookserializable.h"

#define _ __attribute__((unused))

typedef struct {
	gchar* name;
	gchar* serialized;

	gint width;
	gint height;
} MdNotebookUnknownWidgetPrivate;

static void mdnotebook_unknownwidget_serializable_iface_init(MdNotebookSerializableInterface* iface);
G_DEFINE_TYPE_WITH_CODE (MdNotebookUnknownWidget, mdnotebook_unknownwidget, GTK_TYPE_WIDGET,
						 G_IMPLEMENT_INTERFACE(MDNOTEBOOK_TYPE_SERIALIZABLE, mdnotebook_unknownwidget_serializable_iface_init)
						 G_ADD_PRIVATE(MdNotebookUnknownWidget))

static void mdnotebook_unknownwidget_object_dispose(GObject* object) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(MDNOTEBOOK_UNKNOWNWIDGET(object));

	g_free(g_steal_pointer(&priv->name));
	g_free(g_steal_pointer(&priv->serialized));

	G_OBJECT_CLASS(mdnotebook_unknownwidget_parent_class)->dispose(object);
}

static GtkSizeRequestMode mdnotebook_unknownwidget_widget_get_request_mode(GtkWidget*) {
	return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}
static void mdnotebook_unknownwidget_widget_measure(GtkWidget* widget, GtkOrientation orientation, _ int for_size, int* min, int* nat, int* min_baseline, int* nat_baseline) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(MDNOTEBOOK_UNKNOWNWIDGET(widget));

	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		*min = priv->width;
		*nat = priv->width;
	}
	if (orientation == GTK_ORIENTATION_VERTICAL) {
		*min = priv->height;
		*nat = priv->height;
		if (*min >= 0)
			*min_baseline = 0;
		if (*nat >= 0)
			*nat_baseline = 0;
	}
}
static void mdnotebook_unknownwidget_widget_snapshot(GtkWidget* widget, GtkSnapshot* snapshot) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(MDNOTEBOOK_UNKNOWNWIDGET(widget));

#if GTK_CHECK_VERSION(4, 12, 0)
	gdouble
		width = gtk_widget_get_width(widget),
		height = gtk_widget_get_height(widget);
#else
	gdouble
		width = gtk_widget_get_allocated_width(widget),
		height = gtk_widget_get_allocated_height(widget);
#endif

	cairo_t* ctx = gtk_snapshot_append_cairo(snapshot, &GRAPHENE_RECT_INIT(0, 0, width, height));

	gchar* cmb = g_strdup_printf("Unknown widget: %s", priv->name);

	cairo_set_source_rgb(ctx, 1, 0, 0);
	cairo_set_font_size(ctx, 10);
	cairo_move_to(ctx, 0, 14);

	cairo_text_extents_t extents;
	cairo_text_extents(ctx, cmb, &extents);
	if (priv->width != extents.width - extents.x_bearing || priv->height != extents.height - extents.y_bearing) {
		priv->width = extents.width - extents.x_bearing;
		priv->height = extents.height - extents.y_bearing;
		gtk_widget_queue_resize(widget);
		goto free;
	}


	cairo_show_text(ctx, cmb);
	cairo_fill(ctx);

free:
	g_free(cmb);
}

static void mdnotebook_unknownwidget_class_init(MdNotebookUnknownWidgetClass* class) {
	G_OBJECT_CLASS(class)->dispose = mdnotebook_unknownwidget_object_dispose;

	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(class);
	widget_class->get_request_mode = mdnotebook_unknownwidget_widget_get_request_mode;
	widget_class->measure = mdnotebook_unknownwidget_widget_measure;
	widget_class->snapshot = mdnotebook_unknownwidget_widget_snapshot;
}

static const gchar* mdnotebook_unknownwidget_serializable_get_serialized_key(MdNotebookSerializable* iface) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(MDNOTEBOOK_UNKNOWNWIDGET(iface));
	return priv->name;
}
static gchar* mdnotebook_unknownwidget_serializable_serialize(MdNotebookSerializable* iface) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(MDNOTEBOOK_UNKNOWNWIDGET(iface));
	return priv->serialized;
}
static void mdnotebook_unknownwidget_serializable_iface_init(MdNotebookSerializableInterface* iface) {
	iface->get_serialized_key = mdnotebook_unknownwidget_serializable_get_serialized_key;
	iface->serialize = mdnotebook_unknownwidget_serializable_serialize;
}

static void mdnotebook_unknownwidget_init(MdNotebookUnknownWidget* self) {
	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(self);
	priv->width = -1;
	priv->height = -1;
}

GtkWidget* mdnotebook_unknownwidget_new(gchar* name, gchar* serialized) {
	MdNotebookUnknownWidget* self = g_object_new(MDNOTEBOOK_TYPE_UNKNOWNWIDGET, NULL);

	MdNotebookUnknownWidgetPrivate* priv = mdnotebook_unknownwidget_get_instance_private(self);
	priv->name = name;
	priv->serialized = serialized;

	return GTK_WIDGET(self);
}
