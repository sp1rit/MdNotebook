#include <gtk/gtk.h>

/* BEGIN GTK DEPRECATION WORKARROUNDS */

// This is a workarround for gtk#6131, where gtk_text_buffer_insert_at_cursor
// needs the text to be followed by a NULL byte as it looses track of length
// somewhere down the line.
static inline void mdnotebook_util_buffer_insert_at_cursor(GtkTextBuffer* buffer, const gchar* text, gint len) {
#if FALSE // TODO: replace with GTK_CHECK_VERSION(major, minor, patch) once a fix is in gtk
	gtk_text_buffer_insert_at_cursor(buffer, text, len);
#else
	gchar* copied = g_strndup(text, len);
	gtk_text_buffer_insert_at_cursor(buffer, copied, len);
	g_free(copied);
#endif
}

static inline void mdnotebook_util_widget_show(GtkWidget* widget) {
#if GTK_CHECK_VERSION(4, 10, 0)
	gtk_widget_set_visible(widget, TRUE);
#else
	gtk_widget_show(widget);
#endif
}

static inline void mdnotebook_util_widget_hide(GtkWidget* widget) {
#if GTK_CHECK_VERSION(4, 10, 0)
	gtk_widget_set_visible(widget, FALSE);
#else
	gtk_widget_hide(widget);
#endif
}
