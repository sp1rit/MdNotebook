#ifndef __MDNOTEBOOKUNKNOWNWIDGET_H__
#define __MDNOTEBOOKUNKNOWNWIDGET_H__

#include <glib-object.h>
#include <gtk/gtk.h>


G_BEGIN_DECLS

#define MDNOTEBOOK_TYPE_UNKNOWNWIDGET (mdnotebook_unknownwidget_get_type())
G_DECLARE_DERIVABLE_TYPE (MdNotebookUnknownWidget, mdnotebook_unknownwidget, MDNOTEBOOK, UNKNOWNWIDGET, GtkWidget)

struct _MdNotebookUnknownWidgetClass {
	GtkWidgetClass parent_class;
};

GtkWidget* mdnotebook_unknownwidget_new(gchar* name, gchar* serialized);

G_END_DECLS

#endif // __MDNOTEBOOKUNKNOWNWIDGET_H__
