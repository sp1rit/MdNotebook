#include "mdnotebookserializable.h"
#define _ __attribute__((unused))

G_DEFINE_INTERFACE (MdNotebookSerializable, mdnotebook_serializable, GTK_TYPE_WIDGET)

static void mdnotebook_serializable_default_init (MdNotebookSerializableInterface* iface) {
	iface->get_serialized_key = NULL;
	iface->serialize = NULL;
	iface->serialize_markdown = NULL;
}

const gchar* mdnotebook_serializable_get_serialized_key(MdNotebookSerializable* self) {
	MdNotebookSerializableInterface* iface;
	g_return_val_if_fail(MDNOTEBOOK_IS_SERIALIZABLE(self), NULL);
	iface = MDNOTEBOOK_SERIALIZABLE_GET_IFACE(self);

	g_return_val_if_fail(iface->get_serialized_key != NULL, NULL);
	return iface->get_serialized_key(self);
}

gchar* mdnotebook_serializable_serialize(MdNotebookSerializable* self) {
	MdNotebookSerializableInterface* iface;
	g_return_val_if_fail(MDNOTEBOOK_IS_SERIALIZABLE(self), NULL);
	iface = MDNOTEBOOK_SERIALIZABLE_GET_IFACE(self);

	g_return_val_if_fail(iface->serialize != NULL, NULL);
	return iface->serialize(self);
}

gchar* mdnotebook_serializeable_serialize_markdown(MdNotebookSerializable* self) {
	MdNotebookSerializableInterface* iface;
	g_return_val_if_fail(MDNOTEBOOK_IS_SERIALIZABLE(self), NULL);
	iface = MDNOTEBOOK_SERIALIZABLE_GET_IFACE(self);

	g_return_val_if_fail(iface->serialize_markdown != NULL, NULL);
	return iface->serialize_markdown(self);
}
